<?php
return [
    'bootstrap' => ['gii'],
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=localhost;dbname=project',
            'username' => 'postgres',
            'password' => '123456',
            'charset' => 'utf8',
            'tablePrefix' => 'testproject_'
        ],
    ],
];
