<?php

namespace console\controllers;

use common\models\City;
use common\models\Forecast;
use common\models\StatsParamForm;
use LSS\XML2Array;
use yii\console\Controller;

class ForecastController extends Controller
{
    private $_url = 'http://quiz.dev.travelinsides.com/forecast/api/getForecast';

    /**
     * @param $start
     * @param $end
     * @return bool
     * @throws \Exception
     */
    public function actionIndex($start, $end)
    {
        $counter = 0;

        $form = new StatsParamForm([
            'start' => $start,
            'end' => $end
        ]);
        if (!$form->validate()) {
            echo $counter;
            return false;
        }

        $cities = $this->getCities();

        foreach ($cities as $city => $cityId) {
            $url = $this->createUrl($start, $end, $city);
            if (($data = $this->getData($url))) {
                foreach ($data['rows']['row'] as $forecast) {
                    if (!$this->isEmptyForecast($forecast)) {
                        if ($city == $forecast['city']) {
                            if ($this->insertForecast($cityId, $forecast['temperature'], $forecast['ts'])) {
                                $counter++;
                            }
                        }
                    }
                }
            }
        }

        echo $counter;
    }

    /**
     * @param $forecast
     * @return bool
     */
    private function isEmptyForecast($forecast)
    {
        if (!isset($forecast['city']) || !isset($forecast['temperature']) || !isset($forecast['ts'])) {
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    private function getCities()
    {
        $cities = [];
        foreach (City::find()->asArray()->all() as $city) {
            $cities[$city['name']] = $city['id'];
        }
        return $cities;
    }

    /**
     * @param $url string
     * @return array
     * @throws \Exception
     */
    private function getData($url)
    {
        /** @var $data array */
        try {
            $xml = file_get_contents($url);
            $data = XML2Array::createArray($xml);
        } catch (\Exception $e) {
            $data = [];
        }
        if (!isset($data['rows']['row'])) {
            return [];
        }
        return $data;
    }

    /**
     * @param $start string
     * @param $end string
     * @param $city string
     * @return string
     */
    private function createUrl($start, $end, $city)
    {
        return $this->_url . '?start=' . $start . '&end=' . $end . '&city=' . urlencode($city);
    }

    /**
     * @param $cityId integer
     * @param $temperature integer
     * @param $whenCreated string
     * @return bool
     */
    private function insertForecast($cityId, $temperature, $whenCreated)
    {
        $forecast = new Forecast([
            'city_id' => $cityId,
            'temperature' => $temperature,
            'when_created' => $whenCreated
        ]);
        if ($forecast->save()) {
            return true;
        }
        return false;
    }
}
