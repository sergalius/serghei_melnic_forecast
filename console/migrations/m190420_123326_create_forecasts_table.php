<?php

use yii\db\Migration;


class m190420_123326_create_forecasts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%forecast}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'temperature' => $this->double()->notNull(),
            'when_created' => $this->integer()->notNull()
        ]);
        $this->createIndex(
            'idx-unique-city_id_when_created',
            '{{%forecast}}',
            ['city_id', 'when_created'],
            true
        );
        $this->addForeignKey(
            'fk-forecast-city_id',
            '{{%forecast}}',
            'city_id',
            '{{%cities}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-unique-city_id_when_created', '{{%forecast}}');
        $this->dropForeignKey('fk-forecast-city_id', '{{%forecast}}');
        $this->dropTable('{{%forecast}}');
    }
}
