<?php

use yii\db\Migration;

/**
 * Class m190420_123535_insert_data_to_countries_table
 */
class m190420_123535_insert_data_to_countries_table extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%countries}}', [
            'name' => 'Russia'
        ]);
        $this->insert('{{%countries}}', [
            'name' => 'USA'
        ]);
    }

    public function safeDown()
    {
        return false;
    }

}
