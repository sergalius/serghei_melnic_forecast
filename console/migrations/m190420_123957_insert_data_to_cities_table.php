<?php

use yii\db\Migration;

/**
 * Class m190420_123957_insert_data_to_cities_table
 */
class m190420_123957_insert_data_to_cities_table extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%cities}}', ['name' => 'Moscow','country_id' => 1]);
        $this->insert('{{%cities}}', ['name' => 'Nizhny Novgorod','country_id' => 1]);
        $this->insert('{{%cities}}', ['name' => 'Saint Petersburg','country_id' => 1]);
        $this->insert('{{%cities}}', ['name' => 'Yekaterinburg','country_id' => 1]);
        $this->insert('{{%cities}}', ['name' => 'Novosibirsk','country_id' => 1]);

        $this->insert('{{%cities}}', ['name' => 'Chicago','country_id' => 2]);
        $this->insert('{{%cities}}', ['name' => 'Houston','country_id' => 2]);
        $this->insert('{{%cities}}', ['name' => 'Los Angeles','country_id' => 2]);
        $this->insert('{{%cities}}', ['name' => 'New York','country_id' => 2]);
        $this->insert('{{%cities}}', ['name' => 'Philadelphia','country_id' => 2]);
    }

    public function safeDown()
    {
       return false;
    }
}
