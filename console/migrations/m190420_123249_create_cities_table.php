<?php

use yii\db\Migration;

class m190420_123249_create_cities_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cities}}', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'name' => $this->text()
        ]);
        $this->addForeignKey(
            'fk-cities-country_id',
            '{{%cities}}',
            'country_id',
            '{{%countries}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-cities-country_id', '{{%cities}}');
        $this->dropTable('{{%cities}}');
    }

}
