<?php
namespace frontend\controllers;

use common\helpers\ConvertCityHelper;
use common\models\City;
use common\models\CitySearch;
use common\models\StatsParamForm;
use toriphes\console\Runner;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionIndex()
    {
        $statsParamForm = new StatsParamForm();
        if(Yii::$app->request->isAjax && $statsParamForm->load(Yii::$app->request->post())){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($statsParamForm);
        }
        $statsParamForm->loadDefaultValue();
        return $this->render('index', [
            'statsParamForm' => $statsParamForm,
        ]);
    }

    public function actionLoadData()
    {
        if (!Yii::$app->request->isAjax) {
            throw new BadRequestHttpException('Bad Request');
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $count = '';
        $runner = new Runner([
            'yiiscript' => realpath(dirname(Yii::getAlias('@app')) . DIRECTORY_SEPARATOR .  'yii'),
            'phpexec' => PHP_BINDIR . DIRECTORY_SEPARATOR . 'php'
        ]);
        $form = new StatsParamForm();
        $form->load(Yii::$app->request->post());
        if (!$form->validate()) {
            return (["message"=> '0 records loaded']);
        }
        $runner->run('forecast/index ' . $form->start . ' ' . $form->end . ' 2>&1', $count);
        return (["message"=> 'From ' . $form->start . ' to ' . $form->end . ' - ' . $count . ' records loaded']);
    }

    public function actionStats()
    {
        $searchModel = new CitySearch();
        $statsParamForm = new StatsParamForm();

        $statsParamForm->load(Yii::$app->request->get());

        if ($statsParamForm->isEmpty()) {
            $statsParamForm->loadDefaultValue();
        }
        $statsParamForm->validate();

        return $this->render('stats', [
            'dataProvider' => $searchModel->search(Yii::$app->request->queryParams, $statsParamForm),
            'statsParamForm' => $statsParamForm
        ]);
    }

    public function actionHistory($city = '')
    {
        /** @var City $model */
        $model = City::find()->findByName(ConvertCityHelper::convertFromUrl($city))->one();
        if (!$model) {
            throw new NotFoundHttpException('Page not found.');
        }
        return $this->render('history', [
            'forecasts' => $model->getForecasts()->orderBy(['when_created' => SORT_DESC])->all(),
            'city' => $model
        ]);
    }

}
