<?php

use \yii\widgets\ActiveForm;
use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $statsParamForm \common\models\StatsParamForm */

\frontend\assets\DateTimePickerAsset::register($this);

$formName = $statsParamForm->formName();

$form = ActiveForm::begin([
    'id' => $formName,
    'method' => 'get',
    'action' => ['site/stats'],
    'validateOnBlur' => false,
    'options' => ['data-pjax' => true]
]);

?>

<?= $form->field($statsParamForm, 'start', [
    'template' => '{label}
                            <div class="input-group date" id="dtpstart">
                                {input}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                            </div>
                            {hint}{error}',
    'options' => [
        'class' => 'form-group col-xs-4 col-md-2'
    ]
])
    ->textInput([
        'readonly' => 'readonly',
        'data-date-format' => 'DD.MM.YYYY'
    ])?>

<?= $form->field($statsParamForm, 'end', [
    'template' => '{label}
                            <div class="input-group date" id="dtpend">
                                {input}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                            </div>
                            {hint}{error}',
    'options' => [
        'class' => 'form-group col-xs-4 col-md-2'
    ]
])
    ->textInput([
        'readonly' => 'readonly',
        'data-date-format' => 'DD.MM.YYYY'
    ])?>

    <div class="form-group col-xs-4 col-md-2">
        <div style="margin-top:25px">
            <?= Html::submitButton('<span class="glyphicon glyphicon-search"></span> Search', ['class' => 'btn btn-success', 'id' => $formName . 'submit']) ?>
        </div>
    </div>

<?php ActiveForm::end() ?>

<?php
$script = "
$(function () {
    $('#dtpstart').datetimepicker({ignoreReadonly: true});
    $('#dtpend').datetimepicker({ignoreReadonly: true});
});
$('#" . $formName . "').on('beforeSubmit', function(e){
    e.preventDefault();
    var form = $('#" . $formName . "');
    var start = form.find('input[name=\"" . $formName . "[start]\"]').val();
    var end = form.find('input[name=\"" . $formName . "[end]\"]').val();
    var url= form.attr('action') + '?" . $formName . "[start]=' + start + '&" . $formName . "[end]=' + end;
    $.pjax.reload({
        history: false,
        url: url, 
        container: '#pjaxstats'
    });
    return false;
})
";
$this->registerJs($script, $this::POS_END);
