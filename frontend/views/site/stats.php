<?php

/* @var $this yii\web\View */
/* @var $dataProvider  yii\data\ActiveDataProvider; */
/* @var $statsParamForm \common\models\StatsParamForm */

$this->title = 'Forecast - Statistics';
$this->params['breadcrumbs'][] = 'Statistics';

use yii\widgets\Pjax;

Pjax::begin([
    'enablePushState' => false,
    'id' => 'pjaxstats'
]);

?>

    <div class="panel panel-default">
        <div class="panel-heading">
            Search
        </div>
        <div class="panel-body">
            <?= $this->render('_stats_param_form', ['statsParamForm' => $statsParamForm])?>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('_stats_grid', ['dataProvider' => $dataProvider])?>
        </div>
    </div>

<?php

Pjax::end();
