<?php

/* @var $this yii\web\View */
/* @var $dataProvider  yii\data\ActiveDataProvider; */

use \yii\helpers\Url;
use \yii\bootstrap\ButtonDropdown;
use  \yii\grid\GridView;
use \common\helpers\ConvertCityHelper;
use \common\helpers\ConvertTemperatureHelper;

?>

<?php
$summPagLayout = '<div class="row"><div class="col-md-6">{summary}</div><div class="col-md-6 text-right">{pager}</div></div>';
?>

<?= GridView::widget([
    'summary' => '<div class="text-muted" style="margin:25px 0 25px;"> Displaying {begin} - {end} of {totalCount} results.</div>',
    'dataProvider' => $dataProvider,
    'layout' => $summPagLayout . '{items}' . $summPagLayout,
    'pager' => [
        'firstPageLabel' => 'First',
        'lastPageLabel' => 'Last',
        'prevPageLabel' => '←',
        'nextPageLabel' => '→'
    ],
    'columns' => [
        'country',
        'city',
        [
            'attribute' => 'maxTemperature',
            'value' => function($data) {
                return $data['maxTemperature'] ? ConvertTemperatureHelper::convertFahrenheitToCelsius($data['maxTemperature']) : null;
            }
        ],
        [
            'attribute' => 'minTemperature',
            'value' => function($data) {
                return $data['minTemperature'] ? ConvertTemperatureHelper::convertFahrenheitToCelsius($data['minTemperature']) : null;
            }
        ],
        [
            'attribute' => 'avgTemperature',
            'value' => function($data) {
                return $data['avgTemperature'] ? ConvertTemperatureHelper::convertFahrenheitToCelsius($data['avgTemperature']) : null;
            }
        ],
        ['class' => 'yii\grid\ActionColumn',
            'template'=>'{view}',
            'buttons'=>[
                'view'=>function ($url, $model) {
                    /** @var $model \common\models\City */
                    $html = ButtonDropdown::widget([
                        'options' => [
                            'class' => 'btn btn-default',
                        ],
                        'label' => 'Action',
                        'dropdown' => [
                            'items' => [
                                [
                                    'label' => 'History',
                                    'url' => Url::to(['site/history', 'city' => ConvertCityHelper::convertToUrl($model['city'])]),
                                    'linkOptions' => ['data-pjax' => 0]
                                ]
                            ]
                        ]
                    ]);
                    return $html;
                },
            ],
        ],
    ]
])?>
