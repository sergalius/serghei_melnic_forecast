<?php

/* @var $this yii\web\View */
/** @var $forecasts  */
/** @var $city \common\models\City */

use \yii\helpers\Url;
use \common\helpers\ConvertTemperatureHelper;

$headerTitle = 'History of ' . $city->name . ' (' . $city->country->name . ')';
$this->title = 'Forecast - ' . $headerTitle;

$this->params['breadcrumbs'][] = ['label' => 'Statistics', 'url' => Url::to(['site/stats'])];
$this->params['breadcrumbs'][] = $city->name;
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <?= $headerTitle ?>
    </div>
    <div class="panel-body">
        <?php
        $cell = 0;
        $start = '';
        foreach ($forecasts as $item) {
            if ($cell == 0) {
                echo '<div class="row">';
            }
            $header = date('F d, Y', $item['when_created']);
            if (!$start) {
                $start = $header;
                $cell++;
                echo '<div class="col-xs-3">';
                echo '<p><strong>' . $start . '</strong></p>';

            }
            if ($start != $header) {
                $start = $header;
                echo '</div>'; // close col-xs-3
                if ($cell == 4) {
                    $cell = 0;
                    echo '</div>'; // close row
                    echo '<div class="row">';
                }
                $cell++;
                echo '<div class="col-xs-3">';
                echo '<p><strong>' . $start . '</strong></p>';
            }
            echo '<p>' . date('H:i:s', $item['when_created']) . ' ' . ConvertTemperatureHelper::convertFahrenheitToCelsius($item['temperature']) . ' &#8451</p>';
        }
        echo '</div>'; //close col-xs-3
        echo '</div>'; //close row
        ?>
    </div>
</div>
