<?php

use \yii\widgets\ActiveForm;
use \yii\helpers\Html;

/* @var $statsParamForm \common\models\StatsParamForm */

\frontend\assets\DateTimePickerAsset::register($this);

$this->title = 'Forecasts - statistics';
$formName = $statsParamForm->formName();
?>

<div class="site-index">

    <div class="panel panel-default">
        <div class="panel-heading">
            Load new data
        </div>
        <div class="panel-body">

            <?php $form = ActiveForm::begin([
                'id' => $formName,
                'method' => 'post',
                'action' => ['site/load-data'],
                'validateOnBlur' => false,
                'enableAjaxValidation' => true,
                'validationUrl' => ['site/index']
            ]); ?>

            <?= $form->field($statsParamForm, 'start', [
                'template' => '{label}
                            <div class="input-group date" id="dtpstart">
                                {input}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                            </div>
                            {hint}{error}',
                'options' => [
                    'class' => 'form-group col-xs-4 col-md-2'
                ]
            ])
                ->textInput([
                    'readonly' => 'readonly',
                    'data-date-format' => 'DD.MM.YYYY'
                ])?>

            <?= $form->field($statsParamForm, 'end', [
                'template' => '{label}
                            <div class="input-group date" id="dtpend">
                                {input}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                            </div>
                            {hint}{error}',
                'options' => [
                    'class' => 'form-group col-xs-4 col-md-2'
                ]
            ])
                ->textInput([
                    'readonly' => 'readonly',
                    'data-date-format' => 'DD.MM.YYYY'
                ])?>

            <div class="form-group col-xs-4 col-md-2">
                <div style="margin-top:25px">
                    <?= Html::submitButton('<span class="glyphicon glyphicon-search"></span> Load', ['class' => 'btn btn-success', 'id' => $formName . 'submit']) ?>
                </div>
            </div>

            <?php ActiveForm::end() ?>

            <?php
            $script = "
$(function () {
    $('#dtpstart').datetimepicker({ignoreReadonly: true});
    $('#dtpend').datetimepicker({ignoreReadonly: true});
});
$('#" . $formName . "').on('beforeSubmit', function(e){
    e.preventDefault();
    var form = $(this);
    var submitButton = $('#" . $formName . "submit');
    $('#messageBox').text('').addClass('hidden');
    submitButton.text('Wait...').prop('disabled', true).removeClass('btn-success').addClass('btn-warning');
    $.ajax({
            type: form.attr('method'),
            dataType: 'json',
            url: form.attr('action'),
            data: form.serialize(),
            success: function(result) {
                console.log(JSON.stringify(result.message));              
                $('#messageBox').text(JSON.stringify(result.message)).removeClass('hidden');
                resetData();           
            },
            error: function(result) {
                resetData();
                alert('Error. Try again later');
            }
        });
    return false;
})
$('#" . $formName . "').on('beforeValidate', function(e){
    $('#messageBox').text('').addClass('hidden');
})
function resetData()
{
    var submitButton = $('#" . $formName . "submit');
    submitButton.text('Load').prop('disabled', false).addClass('btn-success').removeClass('btn-warning');
}
";
            $this->registerJs($script, $this::POS_END);
            ?>
        </div>
    </div>
    <div class="alert alert-info hidden" id="messageBox">

    </div>
</div>
