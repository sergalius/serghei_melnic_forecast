<?php
namespace common\helpers;

class ConvertCityHelper
{
    public static function convertToUrl($city)
    {
        return str_replace(' ', '_', $city);
    }

    public static function convertFromUrl($city)
    {
        return str_replace('_', ' ', $city);
    }
}