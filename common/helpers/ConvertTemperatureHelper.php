<?php
namespace common\helpers;

class ConvertTemperatureHelper
{
    public static function convertFahrenheitToCelsius($value)
    {
        $value = round((($value - 32) * 5) / 9, 2);
        return $value > 0 ? '+' . $value : $value;
    }
}
