<?php

namespace common\models;

use yii\db\ActiveRecord;

class Country extends ActiveRecord
{
    public function rules()
    {
        return [
            ['name', 'trim'],
            ['name', 'required'],
            ['name', 'string'],
            ['name', 'unique']
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::class, ['country_id' => 'id']);
    }

    /**
     * @return CountryQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new CountryQuery(get_called_class());
    }

    public static function tableName()
    {
        return '{{%countries}}';
    }
}
