<?php

namespace common\models;

use yii\base\Model;

class StatsParamForm extends Model
{
    public $start;
    public $end;

    public function rules()
    {
        return [
            [['start', 'end'], 'required'],
            [['start', 'end'], 'date', 'format' => 'php:d.m.Y'],
            [['start', 'end'], 'checkFormat'],
            ['start', 'checkStartMoreEnd'],
            ['end', 'checkEndMoreStart'],
        ];
    }

    public function isEmpty()
    {
        if (!$this->start && !$this->end) {
            return true;
        }
        return false;
    }

    public function loadDefaultValue()
    {
        $this->start = date("d.m.Y", mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')));
        $this->end = date("d.m.Y");
    }

    public function attributeLabels()
    {
        return [
            'start' => 'Start',
            'end' => 'End',
        ];
    }

    public function checkFormat($attribute)
    {
        if ($this->$attribute != date('d.m.Y', strtotime($this->$attribute))) {
            $this->addError($attribute, 'The format of ' . $this->getAttributeLabel($attribute) . ' is invalid.');
        }
    }

    public function checkStartMoreEnd($attribute)
    {
        if (strtotime($this->start) > strtotime($this->end)) {
            $this->addError($attribute, 'Start date must be anterior to end date');
        }
    }

    public function checkEndMoreStart($attribute)
    {
        if (strtotime($this->end) < strtotime($this->start)) {
            $this->addError($attribute, 'End date must be after start date');
        }
    }
}
