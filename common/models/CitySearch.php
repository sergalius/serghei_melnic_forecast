<?php

namespace common\models;

use yii\base\Model;
use yii\data\ArrayDataProvider;

class CitySearch extends City
{

    public function rules()
    {
        return [
            ['country_id', 'safe'],
            ['maxTemperature', 'safe'],
            ['minTemperature', 'safe'],
            ['avgTemperature', 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params, StatsParamForm $statsParamForm)
    {
        $query = City::find()
            ->select([
                City::tableName() . '.name as city',
                Country::tableName() . '.name as country',
                'maxTemperature' => 'max(' . Forecast::tableName() . '.temperature)',
                'minTemperature' => 'min(' . Forecast::tableName() . '.temperature)',
                'avgTemperature' => 'avg(' . Forecast::tableName() . '.temperature)'
            ])
            ->joinWith('forecasts', false)
            ->joinWith('country', false)
            ->groupBy([City::tableName() . '.id', Country::tableName() . '.id']);

        $errorDates = false;
        if (!$statsParamForm->hasErrors()) {
            $query
                ->andWhere(['>=', 'when_created', strtotime($statsParamForm->start . ' 00:00:00')])
                ->andWhere(['<=', 'when_created', strtotime($statsParamForm->end . ' 23:59:59')]);
        } else {
            $errorDates = true;
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $errorDates ? [] : $query->asArray()->all(),
            'pagination' => [
                'pageSize' => 10
            ],
            'sort' => [
                'attributes' => [
                    'country',
                    'city',
                    'maxTemperature',
                    'minTemperature',
                    'avgTemperature'
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

}
