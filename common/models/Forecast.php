<?php

namespace common\models;

use yii\db\ActiveRecord;

class Forecast extends ActiveRecord
{
    public function rules()
    {
        return [
            ['city_id', 'integer'],
            ['city_id', 'required'],
            [
                'city_id',
                'exist',
                'targetClass' => City::className(),
                'targetAttribute' => ['city_id' => 'id']
            ],

            ['temperature', 'double'],
            ['temperature', 'required'],

            ['when_created', 'trim'],
            ['when_created', 'required'],
            ['when_created', 'string'],
            ['when_created', 'unique', 'targetAttribute' => ['when_created', 'city_id']]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return ForecastQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new ForecastQuery(get_called_class());
    }

    public static function tableName()
    {
        return '{{%forecast}}';
    }
}
