<?php

namespace common\models;

use yii\db\ActiveRecord;;

class City extends ActiveRecord
{

    public function rules()
    {
        return [
            ['country_id', 'integer'],
            ['country_id', 'required'],
            [
                'country_id',
                'exist',
                'targetClass' => Country::className(),
                'targetAttribute' => ['country_id' => 'id']
            ],

            ['name', 'trim'],
            ['name', 'required'],
            ['name', 'string'],
            ['name', 'unique', 'targetAttribute' => ['name', 'country_id']]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForecasts()
    {
        return $this->hasMany(Forecast::class, ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    /**
     * @return CityQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new CityQuery(get_called_class());
    }

    public static function tableName()
    {
        return '{{%cities}}';
    }
}
