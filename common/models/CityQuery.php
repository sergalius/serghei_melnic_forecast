<?php

namespace common\models;

use yii\db\ActiveQuery;

class CityQuery extends ActiveQuery
{
    public function findByName($name)
    {
        return $this->andWhere(['name' => $name]);
    }
}
